﻿using Microsoft.EntityFrameworkCore;

namespace POS.Repository
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }
        public DbSet<ProductsEntity> ProductsEntities => Set<ProductsEntity>();
        public DbSet<CategoriesEntity> CategoriesEntities => Set<CategoriesEntity>();
        public DbSet<SuppliersEntity> SuppliersEntities => Set<SuppliersEntity>();
        public DbSet<CustomersEntity> CustomersEntities => Set<CustomersEntity>();
        public DbSet<EmployeesEntity> EmployeesEntities => Set<EmployeesEntity>();
        public DbSet<OrdersEntity> OrdersEntities => Set<OrdersEntity>();
        public DbSet<OrderDetailsEntity> OrderDetailsEntities => Set<OrderDetailsEntity>();
        public DbSet<ShipperEntity> ShipperEntities => Set<ShipperEntity>();
    }
}