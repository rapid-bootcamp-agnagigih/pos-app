﻿using POS.ViewModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace POS.Repository
{
    [Table("tbl_categories")]
    public class CategoriesEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("category_name")]
        public string CategoryName { get; set; }

        [Required]
        [Column("description")]
        public string Description { get; set; }

        public ICollection<ProductsEntity> Products { get; set; }

        public CategoriesEntity(CategoryModel model)
        {
            CategoryName = model.CategoryName;
            Description = model.Description;
        }

        public CategoriesEntity() { }
    }
}
