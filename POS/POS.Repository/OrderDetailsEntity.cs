﻿using POS.ViewModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace POS.Repository
{
    [Table("tbl_order_details")]
    public class OrderDetailsEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("order_id")]
        public int OrderId { get; set; }

        public OrdersEntity Order { get; set; }

        [Column("product_id")]
        public int ProductId { get; set; }

        public ProductsEntity Product { get; set; }

        [Column("unit_price")]
        public double UnitPrice { get; set; }

        [Column("quantity")]
        public int Quantity { get; set; }

        [Column("discount")]
        public double Discount { get; set; }

        public OrderDetailsEntity(OrderDetailModel model)
        {
            Id = model.Id;
            OrderId = model.OrderId;
            ProductId = model.ProductId;
            UnitPrice = model.UnitPrice;
            Quantity = model.Quantity;
            Discount = model.Discount;
        }
        public OrderDetailsEntity()
        {

        }
    }
}
