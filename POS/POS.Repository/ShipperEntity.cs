﻿using POS.ViewModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace POS.Repository
{
    [Table("tbl_shipper")]
    public class ShipperEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("company_name")]
        public string CompanyName { get; set; }

        [Column("phone")]
        public string Phone { get; set; }

        public ICollection<OrdersEntity> Orders { get; set; }
        public ShipperEntity(ShipperModel model)
        {
            Id = model.Id;
            CompanyName = model.CompanyName;
            Phone = model.Phone;
        }

        public ShipperEntity()
        {
        }
    }
}
