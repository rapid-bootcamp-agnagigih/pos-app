﻿using POS.Repository;
using POS.ViewModel;

namespace POS.Service
{
    public class EmployeeService
    {
        private readonly ApplicationContext _context;

        public EmployeeService(ApplicationContext context)
        {
            _context = context;
        }

        private EmployeeModel EntityToModel(EmployeesEntity entity)
        {
            EmployeeModel model = new EmployeeModel();
            model.Id = entity.Id;
            model.LastName = entity.LastName;
            model.FirstName = entity.FirstName;
            model.Title = entity.Title;
            model.TitleOfCourtesy = entity.TitleOfCourtesy;
            model.BirthDate = entity.BirthDate;
            model.HireDate = entity.HireDate;
            model.Address = entity.Address;
            model.City = entity.City;
            model.Region = entity.Region;
            model.PostalCode = entity.PostalCode;
            model.Country = entity.Country;
            model.HomePhone = entity.HomePhone;
            model.Notes = entity.Notes;
            model.ReportTo = entity.ReportTo;
            return model;
        }

        private void ModelToEntity(EmployeeModel model, EmployeesEntity entity)
        {
            entity.LastName = model.LastName;
            entity.FirstName = model.FirstName;
            entity.Title = model.Title;
            entity.TitleOfCourtesy = model.TitleOfCourtesy;
            entity.BirthDate = model.BirthDate;
            entity.HireDate = model.HireDate;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.Region = model.Region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.HomePhone = model.HomePhone;
            entity.Notes = model.Notes;
            entity.ReportTo = model.ReportTo;
        }

        public List<EmployeesEntity> GetEmployees()
        {
            return _context.EmployeesEntities.ToList();
        }

        public void CreateEmployee(EmployeesEntity newEmployee)
        {
            _context.EmployeesEntities.Add(newEmployee);
            _context.SaveChanges();
        }

        public EmployeeModel ReadEmployee(int? id)
        {
            var employee = _context.EmployeesEntities.Find(id);
            return EntityToModel(employee);
        }

        public void UpdateEmployee(EmployeeModel employee)
        {
            var entity = _context.EmployeesEntities.Find(employee.Id);
            ModelToEntity(employee, entity);
            _context.EmployeesEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteEmployee(int? id)
        {
            var entity = _context.EmployeesEntities.Find(id);

            _context.EmployeesEntities.Remove(entity);
            _context.SaveChanges();
        }
    }
}
