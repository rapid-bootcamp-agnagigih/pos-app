﻿using POS.Repository;
using POS.ViewModel;

namespace POS.Service
{
    public class CategoryService
    {
        private readonly ApplicationContext _context;

        private CategoryModel EntityToModel(CategoriesEntity entity)
        {
            CategoryModel result = new CategoryModel();
            result.Id = entity.Id;
            result.CategoryName = entity.CategoryName;
            result.Description = entity.Description;

            return result;
        }

        private void ModelToEntity(CategoryModel model, CategoriesEntity entity)
        {
            entity.CategoryName = model.CategoryName;
            entity.Description = model.Description;
        }
        public CategoryService(ApplicationContext context)
        {
            _context = context;
        }

        public List<CategoriesEntity> GetCategories()
        {
            return _context.CategoriesEntities.ToList();
        }

        public void CreateCategory(CategoriesEntity newCategory)
        {
            _context.CategoriesEntities.Add(newCategory);
            _context.SaveChanges();
        }

        public CategoryModel ReadCategory(int? id)
        {
            var category = _context.CategoriesEntities.Find(id);
            return EntityToModel(category);
        }

        public void UpdateCategory(CategoryModel category)
        {
            var entity = _context.CategoriesEntities.Find(category.Id);
            ModelToEntity(category, entity);
            _context.CategoriesEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteCategory(int? id)
        {
            var entity = _context.CategoriesEntities.Find(id);

            _context.CategoriesEntities.Remove(entity);
            _context.SaveChanges();
        }

    }
}