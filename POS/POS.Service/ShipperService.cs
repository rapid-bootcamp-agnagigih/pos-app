﻿using POS.Repository;
using POS.ViewModel;

namespace POS.Service
{
    public class ShipperService
    {
        private readonly ApplicationContext _context;

        public ShipperService(ApplicationContext context)
        {
            _context = context;
        }

        private ShipperModel EntityToModel(ShipperEntity entity)
        {
            var model = new ShipperModel();
            model.Id = entity.Id;
            model.CompanyName = entity.CompanyName;
            model.Phone = entity.Phone;
            return model;
        }

        private void ModelToEntity(ShipperModel model, ShipperEntity entity)
        {
            entity.CompanyName = model.CompanyName;
            entity.Phone = model.Phone;
        }

        public List<ShipperEntity> GetShippers()
        {
            return _context.ShipperEntities.ToList();
        }

        public void CreateShipper(ShipperEntity newShipper)
        {
            _context.ShipperEntities.Add(newShipper);
            _context.SaveChanges();
        }

        public ShipperModel ReadShipper(int? id)
        {
            var shipper = _context.ShipperEntities.Find(id);
            return EntityToModel(shipper);
        }

        public void UpdateShipper(ShipperModel shipper)
        {
            var entity = _context.ShipperEntities.Find(shipper.Id);
            ModelToEntity(shipper, entity);
            _context.ShipperEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteShipper(int? id)
        {
            var entity = _context.ShipperEntities.Find(id);

            _context.ShipperEntities.Remove(entity);
            _context.SaveChanges();
        }
    }
}
