﻿using POS.Repository;
using POS.ViewModel;

namespace POS.Service
{
    public class CustomerService
    {
        private readonly ApplicationContext _context;

        private CustomerModel EntityToModel(CustomersEntity entity)
        {
            CustomerModel result = new CustomerModel();
            result.Id = entity.Id;
            result.Name = entity.Name;
            result.ContactName = entity.ContactName;
            result.ContactTitle = entity.ContactTitle;
            result.Address = entity.Address;
            result.City = entity.City;
            result.Region = entity.Region;
            result.PostalCode = entity.PostalCode;
            result.Country = entity.Country;
            result.Phone = entity.Phone;
            result.Fax = entity.Fax;

            return result;
        }

        private void ModelToEntity(CustomerModel model, CustomersEntity entity)
        {
            entity.Name = model.Name;
            entity.ContactName = model.ContactName;
            entity.ContactTitle = model.ContactTitle;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.Region = model.Region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
        }

        public CustomerService(ApplicationContext context)
        {
            _context = context;
        }

        public List<CustomersEntity> GetCustomers()
        {
            return _context.CustomersEntities.ToList();
        }

        public void CreateCustomer(CustomersEntity newCustomer)
        {
            _context.CustomersEntities.Add(newCustomer);
            _context.SaveChanges();
        }

        public CustomerModel ReadCustomer(int? id)
        {
            var customer = _context.CustomersEntities.Find(id);
            return EntityToModel(customer);
        }

        public void UpdateCustomer(CustomerModel customer)
        {
            var entity = _context.CustomersEntities.Find(customer.Id);
            ModelToEntity(customer, entity);
            _context.CustomersEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteCustomer(int? id)
        {
            var customer = _context.CustomersEntities.Find(id);

            _context.CustomersEntities.Remove(customer);
            _context.SaveChanges();
        }


    }
}
