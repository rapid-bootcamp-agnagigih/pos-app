﻿using POS.Repository;
using POS.ViewModel;
using POS.ViewModel.Response;

namespace POS.Service
{
    public class ProductService
    {
        private readonly ApplicationContext _context;

        private ProductModel EnitityToModel(ProductsEntity entity)
        {
            ProductModel result = new ProductModel();
            result.Id = entity.Id;
            result.ProductName = entity.ProductName;
            result.SupplierId = entity.SupplierId;
            result.CategoryId = entity.CategoryId;
            result.Quantity = entity.Quantity;
            result.UnitPrice = entity.UnitPrice;
            result.UnitInStock = entity.UnitInStock;
            result.UnitInOrder = entity.UnitInOrder;
            result.ReorderLevel = entity.ReorderLevel;
            result.Discontinued = entity.Discontinued;

            return result;
        }

        private ProductResponse EnitityToModelResponse(ProductsEntity entity)
        {
            ProductResponse result = new ProductResponse();
            var category = _context.CategoriesEntities.Find(entity.CategoryId);
            var supplier = _context.SuppliersEntities.Find(entity.SupplierId);

            result.Id = entity.Id;
            result.ProductName = entity.ProductName;

            result.CompanyName = supplier.CompanyName;

            result.CategoryName = category.CategoryName;

            result.Quantity = entity.Quantity;
            result.UnitPrice = entity.UnitPrice;
            result.UnitInStock = entity.UnitInStock;
            result.UnitInOrder = entity.UnitInOrder;
            result.ReorderLevel = entity.ReorderLevel;
            result.Discontinued = entity.Discontinued;

            return result;
        }

        private ProductDetailResponse EnitityToModelDetailResponse(ProductsEntity entity)
        {
            ProductDetailResponse result = new ProductDetailResponse();
            var category = _context.CategoriesEntities.Find(entity.CategoryId);
            var supplier = _context.SuppliersEntities.Find(entity.SupplierId);

            result.Id = entity.Id;
            result.ProductName = entity.ProductName;

            result.CompanyName = supplier.CompanyName;
            result.ContactName = supplier.ContactName;
            result.ContactTitle = supplier.ContactTitle;
            result.Phone = supplier.Phone;
            result.HomePage = supplier.HomePage;

            result.CategoryName = category.CategoryName;

            result.Quantity = entity.Quantity;
            result.UnitPrice = entity.UnitPrice;
            result.UnitInStock = entity.UnitInStock;
            result.UnitInOrder = entity.UnitInOrder;
            result.ReorderLevel = entity.ReorderLevel;
            result.Discontinued = entity.Discontinued;

            return result;
        }

        private void ModelToEntity(ProductModel model, ProductsEntity entity)
        {
            entity.ProductName = model.ProductName;
            entity.SupplierId = model.SupplierId;
            entity.CategoryId = model.CategoryId;
            entity.Quantity = model.Quantity;
            entity.UnitPrice = model.UnitPrice;
            entity.UnitInStock = model.UnitInStock;
            entity.UnitInOrder = model.UnitInOrder;
            entity.ReorderLevel = model.ReorderLevel;
            entity.Discontinued = model.Discontinued;
        }

        public ProductService(ApplicationContext context)
        {
            _context = context;
        }

        public List<ProductResponse> GetProducts()
        {
            List<ProductsEntity> products = _context.ProductsEntities.ToList();
            List<ProductResponse> responses = new List<ProductResponse>();
            for (int i = 0; i < products.Count; i++)
            {
                responses.Add(EnitityToModelResponse(products[i]));
            }
            return responses;
        }

        public void CreateProduct(ProductsEntity newProduct)
        {
            _context.ProductsEntities.Add(newProduct);
            _context.SaveChanges();
        }

        public ProductModel ReadProduct(int? id)
        {
            var product = _context.ProductsEntities.Find(id);
            return EnitityToModel(product);
        }

        public ProductDetailResponse ReadProductResponse(int? id)
        {
            var product = _context.ProductsEntities.Find(id);
            return EnitityToModelDetailResponse(product);
        }

        public void UpdateProduct(ProductModel product)
        {
            var entity = _context.ProductsEntities.Find(product.Id);
            ModelToEntity(product, entity);
            _context.ProductsEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteProduct(int? id)
        {
            var product = _context.ProductsEntities.Find(id);

            _context.ProductsEntities.Remove(product);
            _context.SaveChanges();
        }
    }
}
