﻿using POS.Repository;
using POS.ViewModel;

namespace POS.Service
{
    public class SupplierService
    {
        private readonly ApplicationContext _context;

        private SupplierModel EntityToModel(SuppliersEntity entity)
        {
            SupplierModel result = new SupplierModel();
            result.Id = entity.Id;
            result.CompanyName = entity.CompanyName;
            result.ContactName = entity.ContactName;
            result.ContactTitle = entity.ContactTitle;
            result.Address = entity.Address;
            result.City = entity.City;
            result.Region = entity.Region;
            result.PostalCode = entity.PostalCode;
            result.Country = entity.Country;
            result.Phone = entity.Phone;
            result.Fax = entity.Fax;
            result.HomePage = entity.HomePage;

            return result;
        }

        private void ModelToEntity(SupplierModel model, SuppliersEntity entity)
        {
            entity.CompanyName = model.CompanyName;
            entity.ContactName = model.ContactName;
            entity.ContactTitle = model.ContactTitle;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.Region = model.Region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
            entity.HomePage = model.HomePage;
        }

        public SupplierService(ApplicationContext context)
        {
            _context = context;
        }

        public List<SuppliersEntity> GetSuppliers()
        {
            return _context.SuppliersEntities.ToList();
        }

        public void CreateSupplier(SuppliersEntity newSupplier)
        {
            _context.SuppliersEntities.Add(newSupplier);
            _context.SaveChanges();
        }

        public SupplierModel ReadSupplier(int? id)
        {
            var supplier = _context.SuppliersEntities.Find(id);
            return EntityToModel(supplier);
        }

        public void UpdateSupplier(SupplierModel supplier)
        {
            var entity = _context.SuppliersEntities.Find(supplier.Id);
            ModelToEntity(supplier, entity);
            _context.SuppliersEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteSupplier(int? id)
        {
            var supplier = _context.SuppliersEntities.Find(id);

            _context.SuppliersEntities.Remove(supplier);
            _context.SaveChanges();
        }
    }
}
