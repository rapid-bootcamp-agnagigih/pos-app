﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly EmployeeService _service;

        public EmployeeController(ApplicationContext context)
        {
            _service = new EmployeeService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var Employees = _service.GetEmployees();
            return View(Employees);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AddModal()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save(
            [Bind("LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Notes, ReportTo")] EmployeeModel request)
        {
            if (ModelState.IsValid)
            {
                _service.CreateEmployee(new EmployeesEntity(request));
                return Redirect("Index");
            }
            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var employee = _service.ReadEmployee(id);
            return View(employee);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var employee = _service.ReadEmployee(id);
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(
            [Bind("Id, LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Notes, ReportTo")] EmployeeModel request)
        {
            if (ModelState.IsValid)
            {
                _service.UpdateEmployee(request);
                return Redirect("Index");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.DeleteEmployee(id);
            return Redirect("/Employee");
        }
    }
}
