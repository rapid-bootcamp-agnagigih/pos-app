﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class SupplierController : Controller
    {
        private readonly SupplierService _service;

        public SupplierController(ApplicationContext context)
        {
            _service = new SupplierService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var suppliers = _service.GetSuppliers();
            return View(suppliers);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AddModal()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save(
            [Bind("CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, HomePage")] SupplierModel request)
        {
            if (ModelState.IsValid)
            {
                _service.CreateSupplier(new SuppliersEntity(request));
                return Redirect("Index");
            }

            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var supplier = _service.ReadSupplier(id);
            return View(supplier);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var supplier = _service.ReadSupplier(id);
            return View(supplier);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(
            [Bind("Id, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, HomePage")] SupplierModel request)
        {
            if (ModelState.IsValid)
            {
                _service.UpdateSupplier(request);
                return Redirect("Index");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.DeleteSupplier(id);
            return Redirect("/Supplier");
        }

    }
}
