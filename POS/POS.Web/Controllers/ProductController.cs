﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductService _productService;
        private readonly CategoryService _categoryService;
        private readonly SupplierService _supplierService;

        public ProductController(ApplicationContext context)
        {
            _productService = new ProductService(context);
            _categoryService = new CategoryService(context);
            _supplierService = new SupplierService(context);
        }


        [HttpGet]
        public IActionResult Index()
        {
            var products = _productService.GetProducts();
            return View(products);
        }

        [HttpGet]
        public IActionResult Add()
        {
            ViewBag.Category = new SelectList(_categoryService.GetCategories(), "Id", "CategoryName");
            ViewBag.Supplier = new SelectList(_supplierService.GetSuppliers(), "Id", "CompanyName");
            return View();
        }

        [HttpGet]
        public IActionResult AddModal()
        {
            ViewBag.Category = new SelectList(_categoryService.GetCategories(), "Id", "CategoryName");
            ViewBag.Supplier = new SelectList(_supplierService.GetSuppliers(), "Id", "CompanyName");
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save(
            [Bind("ProductName, SupplierId, CategoryId, Quantity, UnitPrice, UnitInStock, UnitInOrder, ReorderLevel, Discontinued")] ProductModel request)
        {
            if (ModelState.IsValid)
            {
                _productService.CreateProduct(new ProductsEntity(request));
                return Redirect("Index");
            }
            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            /*var product = _service.ReadProduct(id);*/
            var product = _productService.ReadProductResponse(id);
            return View(product);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            ViewBag.Category = new SelectList(_categoryService.GetCategories(), "Id", "CategoryName");
            ViewBag.Supplier = new SelectList(_supplierService.GetSuppliers(), "Id", "CompanyName");
            var product = _productService.ReadProduct(id);
            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(
            [Bind("Id, ProductName, SupplierId, CategoryId, Quantity, UnitPrice, UnitInStock, UnitInOrder, ReorderLevel, Discontinued")] ProductModel request)
        {
            if (ModelState.IsValid)
            {
                _productService.UpdateProduct(request);
                return Redirect("Index");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _productService.DeleteProduct(id);
            return Redirect("/Supplier");
        }
    }
}
