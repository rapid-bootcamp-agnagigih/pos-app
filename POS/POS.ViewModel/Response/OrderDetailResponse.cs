﻿namespace POS.ViewModel.Response
{
    public class OrderDetailResponse
    {
        public int Id { get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public double UnitPrice { get; set; }

        public int Quantity { get; set; }

        public double Discount { get; set; }

        public double Subtotal { get; set; }
    }
}
